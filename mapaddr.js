var mongoose = require('mongoose');

var mapaddrSchema = new mongoose.Schema({ 
    features: [{
        f_type: String,
        geometry: {
          g_type: String,
          coordinates: [
             String,
             String
          ]
        },
        properties: {
          phoneFormatted: String,
          phone: String,
          address: String,
          city: String,
          country: String,
          crossStreet: String,
          postalCode: String,
          state: String
        }
    }]
});
module.exports = mongoose.model('mapaddr', mapaddrSchema);