
var mongoose = require('mongoose');

var CounterSchema = new mongoose.Schema({
	"id":{type: String, required: true},
  "sequence_value": { type: Number, default: 0 }
});



module.exports = mongoose.model('counter', CounterSchema);