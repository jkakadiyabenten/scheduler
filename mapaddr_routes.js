const express = require('express');
const router = express.Router();

// Require the controllers WHICH WE DID NOT CREATE YET!!
const mapaddr_controller = require('./mapaddr_controller');
const schedule_controller = require('./schedule_controller');


// a simple test url to check that all of our files are communicating correctly.

router.get('/test', mapaddr_controller.map_details);

module.exports = router;