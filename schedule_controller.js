const Scheduler = require('./schedule');

exports.index_page = function (req, res) {
    res.render('schedule');
};

exports.schedule_details = function (req, res) {
    Scheduler.findById("5b8e88dda7de280a3097de31", function (err, scheduler) {
		if (err) return next(err);
		console.log("scheduler in get",scheduler);
        res.json({models:scheduler});
    })
};

exports.deleteschedule = function (req, res) {
	
	Scheduler.findByIdAndRemove(req.params.id, req.body, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
};

