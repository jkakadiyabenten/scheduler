var mongoose = require('mongoose');

var scheduleSchema = new mongoose.Schema({
		cust: [
			{
				models: [
				{
					id: Number,
					TaskID : {type: Number, default: 1, unique: true},
					OwnerID : Number,
					Title : String,
					Description : String,
					StartTimezone : String,
					Start : String,
					End : String,
					EndTimezone : String,
					RecurrenceRule : String,
					RecurrenceID : String,
					RecurrenceException : String,
					IsAllDay : Boolean
				}
				]
				
			}
		]
});


module.exports = mongoose.model('scheduler', scheduleSchema);
 
