const express = require('express');
var path = require('path');
const app = express();

var bodyParser = require('body-parser');
const mapaddr = require('./mapaddr_routes'); // Imports routes for the products
const schedule = require('./schedule_routes'); // Imports routes for the products
// initialize our express app
app.set('view engine', 'ejs')

var mongoose = require('mongoose');

mongoose.Promise = require('bluebird');
//mongodb://199.127.62.234:2915/kuleanet
mongoose.connect('mongodb://localhost/best4baby', { promiseLibrary: require('bluebird') })
  .then(() =>  console.log('connection successful'))
  .catch((err) => console.error(err));


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use('/img', express.static(__dirname + '/public/img'));

app.use('/', schedule);
app.use('/mapaddr', mapaddr);
app.use('/schedule', schedule);
/* 
app.get('/', function (req, res) {
	
  res.render('index',{test:"Jigna"})
})
 */
app.listen(3000, function () {
  console.log('Example app listening on port 3000!')
})

app.set('view engine', 'ejs')
