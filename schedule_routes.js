const express = require('express');
const router = express.Router();
var mongoose = require('mongoose');


const schedule_controller = require('./schedule_controller');
var Schedule = require('./schedule');

router.get('/', schedule_controller.index_page);
router.get('/getSchedule',schedule_controller.schedule_details);
//router.post('/create',schedule_controller.addschedule);

/* function getNextSequenceValue(sequenceName){
	console.log("in auto increment");
	var cnt = counter.findByIdAndUpdate(
	   {_id: sequenceName}, 
		{$inc:{sequence_value:1}},
		function(err, data){
			console.log("data in get next:",data);
		}
	);	
   console.log("cnt in getNextSequecevalue", cnt.sequence_value);
   return cnt.sequence_value;
} */

router.post('/create', function(req, res) {
	const schedule = req.body;
	var temp;
	Schedule.find(function (err, sch) {
      if (err) return next(err);
	  
	  temp = sch[0].cust[0];
	  console.log("temp.models in if",temp);
		if(temp == undefined) {
		console.log("In create of schedule_routes",schedule);
		Schedule.update({"_id" : mongoose.Types.ObjectId('5b8e88dda7de280a3097de31')},{"$push":{"cust": [{
			"models": [{ 
				"_id": new mongoose.Types.ObjectId(),
                "TaskID": 1,
                "Title": schedule.Title,
				"Start": schedule.Start,
				"End": schedule.End,
				"StartTimezone": schedule.StartTimezone,
				"EndTimezone": schedule.EndTimezone,
				"Description": schedule.Description,
				"RecurrenceID": schedule.RecurrenceID,
				"RecurrenceRule": schedule.RecurrenceRule,
				"RecurrenceException": schedule.RecurrenceException,
				"OwnerID": schedule.OwnerID
				}]
		}]
			
		
            
			}}, (err, createdSchedule) => {
				console.log("createdSchedule",createdSchedule);
				if (err) return res.json({success: false, msg: err});
				res.json({models: [createdSchedule]});
			  }
			);
	} else {
		console.log("temp.models",temp.models);
		
		Schedule.update({"_id" : mongoose.Types.ObjectId('5b8e88dda7de280a3097de31')},{"$push":{"cust.0.models": 
			[{ 
				"_id": new mongoose.Types.ObjectId(),
                "TaskID": 1,
                "Title": schedule.Title,
				"Start": schedule.Start,
				"End": schedule.End,
				"StartTimezone": schedule.StartTimezone,
				"EndTimezone": schedule.EndTimezone,
				"Description": schedule.Description,
				"RecurrenceID": schedule.RecurrenceID,
				"RecurrenceRule": schedule.RecurrenceRule,
				"RecurrenceException": schedule.RecurrenceException,
				"OwnerID": schedule.OwnerID
				}]
		
			
		
            
			}}, (err, createdSchedule) => {
				console.log("createdSchedule",createdSchedule);
				if (err) return res.json({success: false, msg: err});
				res.json({models: [createdSchedule]});
			  }
			)
		
		
		/* console.log("create in controller for existing id",schedule);
		Schedule.update(
		{"cust.0.models":mongoose.Types.ObjectId(schedule._id)},
		{"$set":{
             
                "cust.0.models.$.id": schedule.id,
                "cust.0.models.$.TaskID": schedule.TaskID,
                "cust.0.models.$.Title": schedule.Title,
				"cust.0.models.$.Start": schedule.Start,
				"cust.0.models.$.End": schedule.End,
				"cust.0.models.$.StartTimezone": schedule.StartTimezone,
				"cust.0.models.$.EndTimezone": schedule.EndTimezone,
				"cust.0.models.$.Description": schedule.Description,
				"cust.0.models.$.RecurrenceID": schedule.RecurrenceID,
				"cust.0.models.$.RecurrenceRule": schedule.RecurrenceRule,
				"cust.0.models.$.RecurrenceException": schedule.RecurrenceException,
				"cust.0.models.$.OwnerID": schedule.OwnerID
			}}, (err, createdSchedule) => {
				console.log("createdSchedule",createdSchedule);
				if (err) return res.json({success: false, msg: err});
				res.json({models: [createdSchedule]});
			  }
			); */
	}
	
	  
	  
    });
	
	//console.log(schedule);
	
	
	//console.log("schedule in scheduke_routes",schedule);
	
  });
  
  /* 
  Syntax for findandmodify, also Mongodb can only store one positional variavle '$' for nested query use foreach
  
  db.coll.find().forEach(function( doc ) {
  //supposing only one country will match the query 
    var statesCount = doc.countries[0].states.length;
    for(var i=0; i<statesCount; i++){
        if(doc.countries[0].states[i].state == "Kerala"){
            doc.countries[0].states[i].state = "Goa";
        }       
    }
    db.coll.save(doc);
});

  
  db.getCollection('test').findAndModify(
{
query: { $and: [{_id: ObjectId("5b8ee92bfd38813c8b6f65bb")},{"cust.0._id":ObjectId("5b8ee92bfd38813c8b6f65cc")},
{"cust.0.models._id":ObjectId("5b8ee92bfd38813c8b6f65ce")}] },
update: {$set: { "cust.0.models.1.name": "Test" }},
}

) */
router.post('/update', function(req, res) {
	console.log("update in controller",req.body);
	const schedule = req.body;
	Schedule.findOneAndUpdate(
		{"cust.0.models._id":mongoose.Types.ObjectId(schedule._id)},
		{"$set":{
                
                "cust.0.models.$.TaskID": schedule.TaskID,
                "cust.0.models.$.Title": schedule.Title,
				"cust.0.models.$.Start": schedule.Start,
				"cust.0.models.$.End": schedule.End,
				"cust.0.models.$.StartTimezone": schedule.StartTimezone,
				"cust.0.models.$.EndTimezone": schedule.EndTimezone,
				"cust.0.models.$.Description": schedule.Description,
				"cust.0.models.$.RecurrenceID": schedule.RecurrenceID,
				"cust.0.models.$.RecurrenceRule": schedule.RecurrenceRule,
				"cust.0.models.$.RecurrenceException": schedule.RecurrenceException,
				"cust.0.models.$.OwnerID": schedule.OwnerID
			}}, 
		{new:true},
		function (err, doc) {
				console.log("doc in upate",doc);
				if (err) return res.json({success: false, msg: err});
				res.json({models: [doc]});
			  }
			);
});

// example https://stackoverflow.com/questions/5809788/how-do-i-remove-documents-using-node-js-mongoose
router.post('/delete', function(req, res) {
	console.log("delete in controller",req.body);
	const schedule = req.body;
	Schedule.update(
			{"_id":mongoose.Types.ObjectId("5b8e88dda7de280a3097de31")},
			{$pull:{"cust.0.models":{"_id":mongoose.Types.ObjectId(schedule._id)}}},
			(err, deletedSchedule) => {
				console.log("deletedSchedule",deletedSchedule);
				if (err) return res.json({success: false, msg: err});
				res.json({models: [deletedSchedule]});
		  }
		)
			
});



module.exports = router;


